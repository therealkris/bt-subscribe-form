<?php
/*
Plugin Name: Brian Tracy Sequence Subscription Form
Plugin URI: http://www.briantracy.com
Description: A plugin to add a shortcode with attributes to any page or post that will output a subscription form.
Version: 1.1
Author: Kris Triplett
Author URI: http://www.briantracy.com
*/

  function bt_subscription_form( $atts ) {
    $atts = shortcode_atts( array(
      'label_name'      => 'Your Name',
      'label_email'     => 'Your E-mail Address',
      'label_button'    => 'Submit',
      'error_noemail'   => 'Please enter a valid e-mail address.',
      'ty_page'  => '',
      'webformid'         => '',
      'sequences'       => '',
      'tags'            => ''
    ), $atts );

    extract($atts);

    $cmpid      = isset($_GET['cmpid']) ? $_GET['cmpid'] : '';
    $proid      = isset($_GET['proid']) ? $_GET['proid'] : '';
    $hotid      = isset($_GET['hotid']) ? $_GET['hotid'] : '';
    $aid        = isset($_GET['aid']) ? $_GET['aid'] : '';

    $form = '<form name="bapi_form" action="https://www.briantracy.com/api/submit.aspx" method="POST">
                <input name="cmpid" id="cmpid" value="' . $cmpid . '" type="hidden"/>
                <input name="proid" id="proid" value="' . $proid . '" type="hidden"/>
                <input name="hotid" id="hotid" value="' . $hotid . '" type="hidden"/>
                <input name="aid" id="aid" value="' . $aid . '" type="hidden"/>
                <input name="webformid" id="webformid" type="hidden" value="' . $webformid . '"/>
                <input name="tags" id="tags"  type="hidden" value="' . $tags. '"/>
                <input name="returnurl" type="hidden" id="returnurl" value="' . $ty_page . '"/>
                <input name="sequenceids" type="hidden" id="sequenceids" value=' . $sequences . '"/>
                <input type="text" id="name" name="name" placeholder="' . $label_name . '"/>
                <input type="email" id="email" name="email" placeholder="' . $label_email . '"/>
                <button>' . $label_button . '</button>
            </form>
            <script src="https://www.briantracy.com/js/apitracking.js"></script>';

        return $form;
      } add_shortcode('subscribe', 'bt_subscription_form');